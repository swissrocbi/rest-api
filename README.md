# rest-api

test my REST API solutions

# Recipe App API prooxy

Nginx proxy app for our recipe app

## Usage
### Env variables
* 'LISTEN_PORT'  - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' -
 Port of the app to forward request to (default: '9000')


---

